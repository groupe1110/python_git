#############################
#   FTP Client              #
#   Auteur: Ihrsane VATI    #
#############################

import socket, sys
  
mysock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)


try:
    ipadress = input("Entrez l'adresse IP du serveur : ")
    numport = int(input("Entrez le numéro de port : "))
    mysock.connect((ipadress,numport))

except socket.error:
   print(".......la connexion a échoué.......")
   sys.exit()
       


print(">>> Connexion avec le serveur réussie")
mysock.send(b"Hello je suis connecte")
msgServer=mysock.recv(2048)

print(">>> Serveur :", msgServer.decode())
 
while True:
 
         if msgServer==b'Q':
              break   
               
         msgClient=input(">>> ")
         msgClient=msgClient.encode()
         print(">>> Envoi vers le serveur")
         mysock.send(msgClient)         
         msgServer=mysock.recv(2048)
         print(">>> Reception du serveur")
         print(msgServer.decode())

print (">>> ----Connexion interrompue par le serveur----")
mysock.close()

