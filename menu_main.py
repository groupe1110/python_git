import itertools
import string
import time
import socket 
import threading
import datetime
from queue import Queue



def brute_force():
    # Goes through a list of common passwords
    def guess_common_passwords(password):
        with open('common_password.txt', 'r') as passwords:
            data = passwords.read().splitlines()
            # print(data)

        for i, match in enumerate(data):
            if match == password:
                return f'Le mot de passe est : {match} (tentative #{i})'

        return 0


    # Goes through every combination of chars
    def brute_force(password, min_length=2, max_length=4):
        # Modify this for total symbols
        chars = string.ascii_lowercase + string.digits
        attempts = 0
        for password_length in range(min_length, max_length):
            for guess in itertools.product(chars, repeat=password_length):
                attempts += 1
                guess = ''.join(guess)
                if guess == password:
                    return f'Le mot de passe est {guess}. Trouver au bout de {attempts} combinaisons.'
                print(guess, attempts)


    # Tries the common passwords first, then uses the brute force function
    def get_password(password):
        common = guess_common_passwords(password)
        return brute_force(password) if common == 0 else common


    # Get the current timestamp
    start_time = time.time()

    # Find the password
    print(get_password('a1'))

    # Print the time it took
    print(round(time.time() - start_time, 2), 's')

def scan_port():
    hote = '127.0.0.1'
    f=open ('Scan.txt','w')   ##1ier fichier LOG

    #  pour la date utilise ceci
    now=datetime.datetime.now().date()
    print("date :", now.strftime('%A %d %B %y'))

    #  pour la date et le temps utilise ceci
    now1=datetime.datetime.now().time()
    print("Horaire :", now1.strftime('%A %d %y, %H:%M:%S'))


    ##now = time.time()
    ##print("date=======", now)
 
    ##print ("L'heure et la date actuelle:", time.localtime(now))



    f.write ("la date : "  +str(now) +"\n")
    #  ou bien
    f.write(" La date et l'heure :" +now1.strftime('%A %d %y, %H:%M:%S'))
    f.write ("\n")
    f.write ('+-----------+---------------+\n')
    f.write ('|port       |   etat        |\n')
    f.write ('+-----------+---------------+\n')

    print("------------------------------------------------------------------")

    print("|||| Choix 0 : scan precis d'un port choisi au préalable      ||||")

    print("|||| Choix 1 : scan d'etendue de port choisi au préalable     ||||")

    print("|||| Choix 2 : Port ouvert sur un réseau WAN                  ||||")

    print("------------------------------------------------------------------")


    choix = input("Quel est votre choix ? Mon choix : ")


    def scanport():
        port = int(input("Entrez le port à scanner : "))
        for i in [port]:
            try :
      
                connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                connexion_principale.connect((hote, i))
                #connexion_principale.listen(5)
                print(" le port est ouvert ",i)
                f.write("le port TCP "+str (i)+ "  :ouvert \n")
       
            except:
                print ("le port est fermé",i)
                f.write(" le port TCP "+str (i)+ " :fermé \n")




    if choix == '0':
        scanport()
    
    f.close()

##f=open ('Portouvert.txt','w')  ### 2ième fichier
##
##def scanport_tcp():
##    port = ["500"]
##    for a in port:
##        try :
##            connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
##            connexion_principale.connect((hote, a))
##            connexion_principale.listen(20)
##            print("Autre version :")
##            print (" OK open port",a)
##            f.write("le port TCP  "+str (a)+ "  :ouvert \n")
##            connexion_principale.close()
##        except :
##            print ("Exception : port TCP "+str(a)+" fermé")
##            connexion_principale.close()
##        
##f.close()

    f=open ('Portouvert.txt','w')  ### 2ième fichier

    def scanport_range():
        debut = int(input("Entrez le debut de l'etendue : "))
        fin = int(input("Entrez la fin de l'etendue : "))
        for i in range (debut,fin):
            try :
      
                connexion_principale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                connexion_principale.connect((hote, i))
                #connexion_principale.listen(5)
                print(" le port est ouvert ",i)
                #f.write("le port TCP "+str (i)+ "  :ouvert \n")
       
            except:
                print ("le port est fermé",i)
                #f.write(" le port TCP "+str (i)+ " :fermé \n")

    f.close()

    if choix == '1':
        scanport_range()

    f=open ('Portouvert_WAN.txt','w')  ### 3ième fichier
    def scanport_wan():
        
        socket.setdefaulttimeout(0.25)
        print_lock = threading.Lock()

        target = input('Entrez l\'adresse IP de l\'hote à scanner: ')
        t_IP = socket.gethostbyname(target)
        print ('Début du scan: ', t_IP)

        def portscan(port):
           s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
           try:
              con = s.connect((t_IP, port))
              with print_lock:
                 print(port, 'est ouvert')
              con.close()
           except:
              pass

        def threader():
           while True:
              worker = q.get()
              portscan(worker)
              q.task_done()
      
        q = Queue()
        startTime = time.time()
   
        for x in range(100):
           t = threading.Thread(target = threader)
           t.daemon = True
           t.start()
   
        for worker in range(1, 500):
           q.put(worker)
   
        q.join()
        print('Temps écoulé:', time.time() - startTime)


    f.close()
    
    if choix == '2':
        scanport_wan()
        
    

def menu():
    print("[1] Simulation Brute Force")
    print("[2] Scan de port")
    print("[0] Quitter")

menu()
option = int(input("Entrez une valeur : "))

while option != 0:
    if option == 1:
        print("La simuation de Brute Force est lancé ! ")
        brute_force()
        
    elif option == 2:
        print("Le scan de port est lancé ! ")
        scan_port()
        
    else:
        print("Erreur de saisie")

    print()
    menu()
    option = int(input("Entrez une valeur : "))


print("Merci d'avoir utilisé le programme fourni par l'excellent Albert TAZEGUL ! ")
print("Assena elle soule et elle est moche")
    
