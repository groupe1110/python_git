#############################
#   FTP Server              #
#   Auteur: Ihrsane VATI    #
#############################

import socket,sys
from tkinter import *
from tkinter import filedialog
import os

ssock=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# paramètres du serveur
HOST='127.0.0.1'
PORT=2020
TAILLE_BUFFER=2048

ssock.bind((HOST, PORT))

#boucle principale du serveur
while True :
    print(">>> Serveur prêt, en attente d'un client...")

# jusqu'à 3 requêtes de connexion
    ssock.listen(3)  
# établissement de la connexion et affichage des informations de connexion
    (csock,adresse)=ssock.accept()
    print(">>> Connexion client réussie, adresse IP %s, port %s \n" % (adresse[0], adresse[1]))
     
# Introduction au dialogue avec le client
    csock.send(b"Bienvenue sur le serveur\n----Si le menu ne s'affiche pas faites 'Entrer'----\n")
    csock.send(b">>> Voici le menu:\nr : Gestion des repertoires\nf : Gestion des fichiers\nl : Lister les fichiers\nQ : Quitter") 

# réception de message du client
    msgClient=csock.recv(TAILLE_BUFFER)
    print('>>> Client:' ,msgClient.decode())

# boucle d'échange avec le client
# menu d'interaction
    while True :
        if msgClient==b"Q" :
            break
        if msgClient==b"f" :
            filename = filedialog.askopenfilename(initialdir="/", title="Selectionnez un fichier",filetypes = (("Documents Word",".docx"),("Fichier pdf",".pdf"),("All Files","*.*")))
            os.popen(filename)
        if msgClient==b"r" :
            dirname = filedialog.askdirectory()
            print(dirname)
        msgServer=input(">>> ")
        msgServer=msgServer.encode()
        print(">>> Envoi vers le client, en attente de réponse....")
        csock.send(msgServer)             
        msgClient=csock.recv(TAILLE_BUFFER)
        print(">>> Reception du client")
        print(msgClient.decode())

# fermeture de la connexion
    csock.send(b"Vous avez ferme la connexion, au revoir !")
    print(">>> ----connexion interompue par le client----")
    csock.close()
    ch=input("<R>ecommencer <T>erminer?")
    if ch==b'T':
        print('Fin des connexions.')
        break
ssock.close()
  
                   
